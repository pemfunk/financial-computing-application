# Financial Computing Application

Pemfunk Group Members:

1. Ahmad Satryaji Aulia - 1606885864

2. Alya Putri - 1606836471

3. Donny Samuel - 1606918603

4. I Gusti Agung Nanda Dharma Wangsa - 1606917720

5. Raden Fikri Ihza Dwi Nanda - 1606833476

  

## Description

This is a Stock Prediction Application, that will give you prediction on the stock that you trade. This application will automatically compute whether you gain profit or loss from the trade that you did.

  

## Website Design

The mockup for the application is made on Figma, which can be accessed here: [Financial Computing Application Mockup](https://www.figma.com/file/KOB9FjFQJvcekXboe9AsY7/PemFunk-Financial-Computing-Application?node-id=0:1)

## Deployment

Our applications is deployed using DigitalOcean here:
* **Frontend** [https://satraul.com/pemfung/](https://satraul.com/pemfung/)
* **Backend** [http://hs.satraul.com/](http://hs.satraul.com/)