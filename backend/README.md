# Pemfunk Backend

## Installation

Install cabal-install using:
```
$ cabal install cabal-install
```

Build the `pemfunk-backend` package using:
```
$ cd ./backend
$ cabal update
$ cabal install --overwrite-policy=always
```

## Development

Run the package using:
```
$ pemfunk-backend
```

## Deployment

Deploy `pemfunk-backend` using:
```
$ rsync apache2/* root@satraul.com:/etc/apache2/sites-available/
$ rsync -r backend/ satraul.com:~/pemfung
$ ssh satraul.com
$ a2ensite pemfung haskell
$ systemctl reload apache2
$ cd ./backend
$ cabal update
$ cabal install --overwrite-policy=always
$ pemfunk-backend &!
```

## API used

endpoint:
```
https://www.alphavantage.co/query?
```

params:
- funtion:
 TIME_SERIES_INTERDAY | TIME_SERIES_DAILY | TIME_SERIES_WEEKLY
- symbol:
 BBCA | MSFT | AAPL
- interval: 
 60min
- apikey:
 38FA3188TQB48IS9
- outputsize:
 full | compact

exampe api call:
```
https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=BBCA&interval=60min&apikey=38FA3188TQB48IS9&outputsize=compact
```

more on:
https://www.alphavantage.co/documentation/

## API Implemented

```
localhost:8000/<symbol_lowercase>/<time_series>
```

example:
```
localhost:8000/bbca/weekly
```

will return
```
[25.5, 25.56, 25.41, 25.63, 25.32, 25.15, 25.01]
```
7 consecutive-point time series from present to past