{-# LANGUAGE QuasiQuotes, OverloadedStrings #-}

module Mock where

import NeatInterpolation (text)
import qualified Data.Text.IO

bbcaMonth = 
    [text|[25.56, 24.92, 25.02, 24.39, 24.62, 25.10, 23.76]
    |]

bbcaWeek = 
    [text|[25.5, 25.56, 25.41, 25.63, 25.32, 25.15, 25.01]
    |]

bbcaDay =
    [text|[25.50, 25.46, 25.53, 25.36, 25.235, 25.56, 25.61]
    |]

msftMonth = 
    [text|[151.81, 144.26, 139.66, 139.61, 137.00, 136.63, 123.85]
    |]

msftWeek = 
    [text|[151.07, 151.81, 150.00, 150.07, 145.34, 144.83, 144.40]
    |]

msftDay =
    [text|[151.07, 150.99, 150.05, 150.14, 150.49, 151.81, 152.10]
    |]

aaplMonth =
    [text|[267.27, 249.54, 225.07, 206.43, 213.90, 203.17, 175.60]
    |]

aaplWeek =
    [text|[270.00, 267.27, 262.71, 265.80, 258.30, 257.33, 247.42]
    |]

aaplDay = 
    [text|[270.00, 267.48, 263.79, 261.07, 258.31, 267.27, 266.60]
    |]