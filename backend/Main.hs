import Mock

import Control.Monad
import Happstack.Server

main :: IO()
main = simpleHTTP nullConf $ msum
        [ dirs "bbca/month" $ ok (toResponse bbcaMonth)
        , dirs "bbca/week" $ ok (toResponse bbcaWeek)
        , dirs "bbca/day" $ ok (toResponse bbcaDay)
        , dirs "msft/month" $ ok (toResponse msftMonth)
        , dirs "msft/week" $ ok (toResponse msftWeek)
        , dirs "msft/day" $ ok (toResponse msftDay)
        , dirs "aapl/month" $ ok (toResponse aaplMonth)
        , dirs "aapl/week" $ ok (toResponse aaplWeek)
        , dirs "aapl/day" $ ok (toResponse aaplDay)
        , dir "goodbye" $ ok (toResponse "Goodbye, moon!")]
