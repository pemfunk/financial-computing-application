import React from 'react';
import './App.css';
import StockPeriod from './components/StockPeriod'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import StockDisplay from './components/StockDisplay';
import StockOption from './components/StockOption';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function App() {
  // const [open, setOpen] = React.useState(false);

  // const handleClickOpen = () => {
  //   setOpen(true);
  // };
  // const handleClose = () => {
  //   setOpen(false);
  // };

  return (
    <div className="App">
      <div className="App-container">
        <header className="App-header">
          <p>
            Stock Prediction App, created by PemFunk.
          </p>
        </header>

        <div className="App-content">
          <Grid container spacing={3}>
            <Grid item xs={4}>
              <StockOption></StockOption>
            </Grid>
            <Grid item xs={8}>
              <StockPeriod />
            </Grid>
            {/* <StockOption /> */}
          </Grid>
        </div>

        <div>
          <a
            className="App-link"
            href="https://gitlab.com/pemfunk/financial-computing-application"
            target="_blank"
            rel="noopener noreferrer"
          >
            Gitlab
          </a>

        </div>
      </div>
    </div>
  );
}

export default App;



// class App extends React.Component {

//   componentDidMount() {
//     axios.get(`https://jsonplaceholder.typicode.com/users`)
//       .then(res => {
//         console.log(res);
//       })
//   }

//   render() {
//     return (
//     );
//   }
// }

// function App() {
//   componentDidMount(){
//     console.log("mashook")
//   }

// return (
//   <div className="App">
//     <header className="App-header">
//       <p>
//         Stock Prediction App, created by PemFunk.
//       </p>
//       <a
//         className="App-link"
//         href="https://gitlab.com/pemfunk/financial-computing-application"
//         target="_blank"
//         rel="noopener noreferrer"
//       >
//         Gitlab
//       </a>
//     </header>
//     <Chart />
//   </div>
// );
// }

//export default App;
