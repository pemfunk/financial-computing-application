import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import '../styles/Chart.css';
import axios from 'axios';


class Chart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartData: {
                labels: ['07.00 AM', '08.00 AM', '09.00 AM', '10.00 AM',
                    '11.00 AM', '12.00 PM', '01.00 PM'],
                datasets: [
                    {
                        label: 'Stock Price',
                        data: [],
                        backgroundColor: 'rgba(54, 99, 132, 0.6)',
                        borderColor: 'rgba(54, 99, 132, 1)',
                        fill: false,
                        yAxisID: 0,
                        lineTension: 0
                    }
                ]
            }
        }
    }

    render() {
        return (
            <div className="chart" style={{ position: "relative", width: 600, height: 300 }}>
                <Line
                    data={this.state.chartData}
                    options={{
                        responsive: true,
                    }}
                />
            </div>
        )
    }

    componentDidMount() {
        var data = [];
        axios.get(`http://hs.satraul.com/bbca/day`)
            .then(res => {
                for (let i = 0; i < res.data.length; i++) {
                    data.push(res.data[i])
                }
                this.setState({
                    chartData: {
                        labels: ['07.00 AM', '08.00 AM', '09.00 AM', '10.00 AM',
                            '11.00 AM', '12.00 PM', '01.00 PM'],
                        datasets: [
                            {
                                label: 'Stock Price',
                                data: data,
                                backgroundColor: 'rgba(54, 99, 132, 0.6)',
                                borderColor: 'rgba(54, 99, 132, 1)',
                                fill: false,
                                yAxisID: 0,
                                lineTension: 0
                            }
                        ]
                    }
                })
            })
    }
}

export default Chart;