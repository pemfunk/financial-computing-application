import React, { useState } from 'react';
import '../styles/StockOption.css';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import StockDisplay from './StockDisplay';
import { connect } from "react-redux";


function mapStateToProps(state) {
    return {
      stockName: state.stocks,
      stockAmount: state.stockAmount
    };
  }

function StockOption(props) {
    const [title, settitle] = useState("Add Stock")

    const [stocks, setStocks] = useState("BCA")
    const [stockAmount, setStockAmount] = useState("0")

    const [open, setOpen] = React.useState(false);

    console.log("start")
    console.log(props.stocks)
    console.log("finish")

    const createDropdownOptions = (list) => {
        console.log(list.get(0))
        var dropdownOptions = []
        // for (var i = 0; i < list.length ; i++) {
        //     dropdownOptions.push(<option value={list.get(i)}>{list.get(i)}</option>)
        // }
        // list.array.forEach((element) => {
        //     
        // });

        console.log(dropdownOptions)
        return dropdownOptions;
    }

    const updateStock = () => {
        this.props.dispatch({ type: "UPDATESTOCK" });
      };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleStockChange = (event) => {
        const value = event.target.value;
        setStocks(value)
    }

    const handleStockAmountChange = (event) => {
        const value = event.target.value;
        setStockAmount(value)
    }

    const handleSubmit = (evt) => {

        evt.preventDefault();
        alert(`Submitting Stocks ${stocks} and Stock Amount ${stockAmount}`)
        setOpen(false);

        //this.props.dispatch({ type: "UPDATESTOCK" });

    }
    

    // function handleChange(event) {
    //     const name = event.target.name;
    //     const value = event.target.value;

    //     switch(name) {
    //         case "stockAmount" : setStockAmount(value)
    //         case "stocks": setStocks(value)
    //     }
    // }


    return (
        <div>
            <StockDisplay stockname={stocks} stockamount={stockAmount}></StockDisplay>
            <Button className="add-stock-button" variant="contained" onClick={handleOpen} fullWidth>Change Stock</Button>

            <Dialog open={open} onClose={handleClose}>
                <div className="modal-backdrop">
                    <div className="modal-container">
                        <div className="modal-title">
                            <p>{title}</p>
                        </div>
                        <div className="modal-content">
                            <p className="content-title"><b>Stock Name</b></p>
                            <select
                                name="stocks"
                                className="content-object dropdown"
                                value={stocks}
                                onChange={handleStockChange}>
                                <option value="BCA">BCA</option>
                                <option value="Apple">Apple Inc.</option>
                                <option value="Microsoft">Microsoft</option>
                            </select>
                            <br />
                            <br />
                            <p className="content-title"><b>Stock Amount</b></p>
                            <div className="input-amount">
                                <p className="content-title"><b>Rp.</b></p>
                                <input type="number"
                                    name="stockAmount"
                                    className="content-object"
                                    value={stockAmount}
                                    onChange={handleStockAmountChange}></input>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button className="cancel" onClick={handleClose}> Cancel </button>
                            <button type="submit" value="Submit" className="save" onClick={handleSubmit}> Save </button>
                        </div>
                    </div>
                </div>
            </Dialog>

        </div>

    )
}

//function Modal = ({title, content}) => {}
//function dropdown()

export default connect(mapStateToProps)(StockOption);