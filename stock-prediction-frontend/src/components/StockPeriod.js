import React, { Component } from 'react';
import Chart from './Chart';
import ChartMonth from './ChartMonth';
import ChartWeek from './ChartWeek';
import StockDisplay from './StockDisplay';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import '../styles/StockPeriod.css';


class StockPeriod extends Component {
    constructor(props) {
        super(props)

        this.state = {
            chart: <Chart></Chart>
        }
    }

    dailyChart = () => {
        this.setState({
            chart: <Chart></Chart>,
        })
    }

    weeklyChart = () => {
        this.setState({
            chart: <ChartWeek></ChartWeek>,
        })
    }

    monthlyChart = () => {
        this.setState({
            chart: <ChartMonth></ChartMonth>,
        })
    }

    render() {
        return (
            <div>
                <Grid container spacing={1} direction="column" alignItems="center">
                    <Grid item>
                        <ButtonGroup color="primary" aria-label="outlined primary button group">
                            <Button onClick={this.dailyChart}>1D</Button>
                            <Button onClick={this.weeklyChart}>1W</Button>
                            <Button onClick={this.monthlyChart}>1M</Button>
                        </ButtonGroup>
                    </Grid>
                </Grid>
                <div className="chartcontent">
                    {this.state.chart}
                </div>

            </div>
            // <div className="flex">
            //     <div>
            //         <Button className="add-stock-button" variant="contained">+ Add Stock</Button>
            //         <StockDisplay/>
            //     </div>
            //     <div>
            //         <Grid item>
            //             <ButtonGroup variant="text" aria-label="full-width contained primary button group">
            //                 <Button onClick={this.dailyChart}>1D</Button>
            //                 <Button onClick={this.weeklyChart}>1W</Button>
            //                 <Button onClick={this.monthlyChart}>1M</Button>
            //             </ButtonGroup>
            //         </Grid>
            //         {this.state.chart}
            //     </div>
            // </div>
        )
    }
}

export default StockPeriod;