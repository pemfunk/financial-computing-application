import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

import '../styles/StockDisplay.css';

const useStyles = makeStyles({
  card: {
    textAlign: "left",
    borderRadius: "6px",
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  text: {
    fontFamily: "Rubik",
  },
  title: {
    fontFamily: "Rubik",
    fontSize: "20px",
    fontWeight: 700,
  },
  pos1: {
    fontFamily: "Rubik",
    marginTop: 4,
  },

  pos2: {
    marginTop: 2,
    fontFamily: "Rubik",
  },


});

export default function SimpleCard(props) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.card}>
      <CardContent>
        <div>
          <Typography className={classes.title} color="primary">
            {props.stockname}
          </Typography>
          <Typography className={classes.pos1} color="textSecondary">
            Stock Amount: {props.stockamount}
          </Typography>
          {/* <Typography className={classes.pos2} color="textSecondary">
            Profit: xx
        </Typography> */}
        </div>
        {/* <div className="flex-stock-display">
          <div>
            <Typography variant="h5" component="h2">
              Stock Name
        </Typography>
            <Typography className={classes.pos1} color="textSecondary">
              Stock Amount: xx
        </Typography>
            <Typography className={classes.pos2} color="textSecondary">
              Profit: xx
        </Typography>
          </div>
          <div>
            <IconButton aria-label="edit" color="primary">
              <EditIcon></EditIcon>
            </IconButton>
            <IconButton aria-label="delete" color="primary">
              <DeleteIcon></DeleteIcon>
            </IconButton>
          </div>
        </div> */}
      </CardContent>
    </Card>
  );
}