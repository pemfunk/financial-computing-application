import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import axios from 'axios';

class ChartMonth extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartData: {
                labels: [],
                datasets: [
                    {
                        label: 'Stock Price',
                        data: [],
                        backgroundColor: 'rgba(54, 99, 132, 0.6)',
                        borderColor: 'rgba(54, 99, 132, 1)',
                        fill: false,
                        yAxisID: 0,
                        lineTension: 0
                    }
                ]
            }
        }
    }

    render() {
        return (
            <div className="chart" style={{ position: "relative", width: 600, height: 300 }}>
                <Line
                    data={this.state.chartData}
                    options={{
                        responsive: true,
                    }}
                />
            </div>
        )
    }

    componentDidMount() {
        var data = [];
        axios.get(`http://hs.satraul.com/bbca/month`)
            .then(res => {
                for (let i = 0; i < res.data.length; i++) {
                    data.push(res.data[i])
                }
                this.setState({
                    chartData: {
                        labels: ['January', 'February', 'March', 'April',
                            'May', 'June', 'July'],
                        datasets: [
                            {
                                label: 'Stock Price',
                                data: data,
                                backgroundColor: 'rgba(54, 99, 132, 0.6)',
                                borderColor: 'rgba(54, 99, 132, 1)',
                                fill: false,
                                yAxisID: 0,
                                lineTension: 0
                            }
                        ]
                    }
                })
            })
    }
}

export default ChartMonth;