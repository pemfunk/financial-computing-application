import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import WebFont from 'webfontloader';

import { createStore } from 'redux';
import { Provider } from 'react-redux';

const initialState = {
  stockNameStore: "init",
  stockAmountStore: 0,
};

function reducer(state = initialState, action) {
  console.log('reducer', state, action);

  switch(action.type) {
    case 'UPDATESTOCK':
      return {
        stockNameStore: state.stocks,
        stockAmountStore: state.stockAmount
      };
    default:
      return state;
  }
}

const store = createStore(reducer);
store.dispatch({ type: "UPDATESTOCK" });

const rootElement = document.getElementById('root')
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);

WebFont.load({
    google: {
      families: ['Rubik:400,700,900', 'sans-serif']
    }
  });

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
